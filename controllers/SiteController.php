<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;



class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        
        /**
         * La mejor forma de realizar consultas (sobre todo las consultas tipicas)
         * es mediante Active Record
         * Para ello vamos a utilizar la clase yii/db/ActiveRecord
         * Ademas tenemos que modelar todas las tablas
         * para realizar los modelos utilizamos Gii
         * 
         */
        
        $consulta= \app\models\Noticias::find();
        
        /**
         * Debido a que yii\db\ActiveQuery se extiende desde yii\db\Query , puede usar todos los métodos de creación de consultas y métodos de consulta del padre
         */
        
        $salidaArrayObjetos=$consulta->all();
       
            
        return $this->render('index',[
            'noticias'=>$salidaArrayObjetos,
        ]);
    }

}
